import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NavbarComponent} from './navbar/navbar.component';

@NgModule({
  exports: [
    CommonModule,
    NavbarComponent],
  declarations: [NavbarComponent],
})
export class AppCommonModule {

}
